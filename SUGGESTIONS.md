# SUGGESTIONS
Here is a summary of the most recurrent or interesting suggestions  
Items are not prioritized

## Core features
- Remember: invisible text injected into context in each prompt
- WI: invisible text injected into context when keys are found
- Scripting (and be able to import scripts onto any scenario/story)
- Retry button
    - Cache the different retry outputs for the last input
      - Easily select the output that we like the most (displays retries for current input, click to select)
- Multiplayer
- Mobile compatibility/usability
- Characters dedicated creation/memory (with remember of WI) seem to pop up often
- Inventory system (often requested)
- Action buttons (i.e. an "examine area" button) https://discord.com/channels/836774308772446268/837077266918932512/837187576862867466
- Precision mode: only output one sentence at a time for better precision
- Word banlist/blacklist
  - Simple word match and output slicing would be a first solution
  - Logit bias to reduce probability of words/tokens to appear may be implemented too
    - Word whitelist: logit biases can also be used to make some words have a better chance of appearing 
- Import/export prompts/scenario/WI/scripts (pretty much everything, to allow decentralized platform)
- NSFW filters
  - Categorized filters https://discord.com/channels/836774308772446268/837077266918932512/837624456494317589
- "Comment" text in story that the AI don't see (mentioned multiple times) https://discord.com/channels/836774308772446268/837077266918932512/837285834305962035
- User controllable context injection (like AID SimpleContext, EWIJSON, etc.) https://discord.com/channels/836774308772446268/837077266918932512/837511876040196121
  - Information priority system https://discord.com/channels/836774308772446268/837077266918932512/837536617212280932
- Better support for non-human characters (seems a popular opinion) https://discord.com/channels/836774308772446268/837077266918932512/837538427107803166

## User Interface and User Experience (UI/UX)
- Customizable remember section https://discord.com/channels/836774308772446268/837077266918932512/837243585929543700
- Make the UI easily customizable (a lot of other UI request that can be solved by this solution, temporary or not) https://discord.com/channels/836774308772446268/837077266918932512/837333009346592798
- Make the background customizable https://discord.com/channels/836774308772446268/837077266918932512/837477055058280448
- Story pagination https://discord.com/channels/836774308772446268/837077266918932512/837571272354562068
  - And chapter breaks https://discord.com/channels/836774308772446268/837077266918932512/837624456494317589
- "Distraction free mode" that acts as a lightweight interface with only text editing, input and send button
- Custom music https://discord.com/channels/836774308772446268/837077266918932512/837207277990969405
- Different colors for player/AI output https://discord.com/channels/836774308772446268/837077266918932512/837252763145469982
  - User defined text colors (and background color) https://discord.com/channels/836774308772446268/837077266918932512/838126526242226176
- Chapters https://discord.com/channels/836774308772446268/837077266918932512/837388845888569344
- Visual representation of injected context https://discord.com/channels/836774308772446268/837077266918932512/837511876040196121
- Drag and drop importable content https://discord.com/channels/836774308772446268/837077266918932512/838113038358806538
  - Don't make drag&drop the only option https://discord.com/channels/836774308772446268/837077266918932512/838114123584765962
- PDF export button on stories https://discord.com/channels/836774308772446268/837077266918932512/838153098886905857

## Modding and Scripting
An issue has been created to debate scripts/mods suggestions [HERE](https://gitlab.com/nolialsea/novelaicontributions/-/issues/3)

- Context debugging window https://discord.com/channels/836774308772446268/837077266918932512/837513669158895667
  - Lists token, word and character count with limits as applicable https://discord.com/channels/836774308772446268/837077266918932512/837806156768477195
- Multiple scripts at once & load order https://discord.com/channels/836774308772446268/837259157538340864/838107653292294174
- Call scripts "mods" instead https://discord.com/channels/836774308772446268/837259157538340864/838107945829007420
  - https://discord.com/channels/836774308772446268/837077266918932512/838109172380729344
- Drag and drop mods https://discord.com/channels/836774308772446268/837259157538340864/838111910566166579

## Accessibility and Text-to-speech
An issue has been created to debate text-to-speech [HERE](https://gitlab.com/nolialsea/novelaicontributions/-/issues/2)

- **Accessibility for the Blind and Visually Impaired** https://discord.com/channels/836774308772446268/837077266918932512/837778559949865011
  - Custom TTS, and possible 15.ai collaboration? https://discord.com/channels/836774308772446268/837077266918932512/837783051547181116

## World building and other generation thingies
- World building tools https://discord.com/channels/836774308772446268/837077266918932512/837624456494317589
- Custom generators https://discord.com/channels/836774308772446268/837077266918932512/837252396735660092
- Generic generators https://discord.com/channels/836774308772446268/837077266918932512/837397969997004820
- World building presets https://discord.com/channels/836774308772446268/837077266918932512/837774893364805732

## Evaluation bots
(Secondary specialized AIs that analyses input or output and produces a result) 
- Generate suggested actions
- Ability to recognise character perspective changes, scene changes, time skips etc (feasibility study needed) https://discord.com/channels/836774308772446268/837077266918932512/837624456494317589

## Prompts/scenarios storage
- Easy import/export of community content, being scenarios, scripts, World Info (and others?) https://discord.com/channels/836774308772446268/837077266918932512/837778543948857384

## Information and Documentation
- Finance: people ask for information about how it will be financed initially and then on the run
  - We need suggestion for business plan, here is one: https://discord.com/channels/836774308772446268/837077266918932512/837517865410232370
  - many are willing to contribute financially to help quickstart the project
  - Avoid energy system if possible (but also keep it? https://discord.com/channels/836774308772446268/837077266918932512/837502554547814410)
- Timeline/roadmap of the project https://discord.com/channels/836774308772446268/837077266918932512/837475805944217641

## AI related features
- Larger prompts size (aka the 1k token limit in AID, said to be 3k token with current planned model?)
  - Might have some limitations, as increasing "memory" does not mean that the AI will actually remember everything
- First/second/third person story telling are all requested (would depend on model finetuning)
- Finetune with books instead of CYOA: Will need a dedicated suggestion page for suggested books
- Early standalone client for testing purpose on small scale models before upscaling https://discord.com/channels/836774308772446268/837077266918932512/837119941546999849
- Different models
    - Dedicated SFW/NSFW models
    - More specific models/finetunes (SFW, NSFW, cthulhu, scifi, fantasy, real world, kink-specific etc.)
- Early standalone client for testing purpose on small scale models before upscaling https://discord.com/channels/836774308772446268/837077266918932512/837119941546999849
- Establish standards for data formatting (will need a period of research) https://discord.com/channels/836774308772446268/837077266918932512/837245232968892466
- Secondary, lighter model to run simple responses (could be used for evaluation bots too) https://discord.com/channels/836774308772446268/837077266918932512/837259307837423647
  - See `EVALUATION_BOTS_SUGGESTIONS.md`
- Give access to tokenizer https://discord.com/channels/836774308772446268/837077266918932512/837624456494317589
- Avoid "sentence fragment" (don't crop prompts mid sentence) https://discord.com/channels/836774308772446268/837077266918932512/837511876040196121

## Security and privacy
- A lot of security/encryption/privacy discussions and concerns, may need its dedicated suggestion page

## Other
- A lot of discussions about "brand", "logo" and the general image of NovelAI
- Don't rush things (aka proper project management) https://discord.com/channels/836774308772446268/837077266918932512/837468402481102868
- Don't rush things (aka proper code management) https://discord.com/channels/836774308772446268/837077266918932512/837473914493468692
- Avoid false hope (aka the No Man's Sky effect) https://discord.com/channels/836774308772446268/837077266918932512/837476784680206346
- Beta/test environments https://discord.com/channels/836774308772446268/837077266918932512/837644094415175780
  - Toggleable experimental features and blind tests https://discord.com/channels/836774308772446268/837077266918932512/837670430486560828
- Project/team communication https://discord.com/channels/836774308772446268/837077266918932512/837646823660191764

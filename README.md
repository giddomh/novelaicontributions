# NovelAIContributions

An early contribution tool for the NovelAI project

## About this repository
It is an attempt to summarize and centralize community suggestions about NovelAI  
It is maintained by Noli (me), which is not part of the NovelAI discord team  

## I want to contribute
Good! You can clone this repo and make a merge request, I'll be happy to get some activity here!

## Note on this repo ownership
Note to official NovelAI team members that may read this: this repo is **yours**, you can take over whenever you want

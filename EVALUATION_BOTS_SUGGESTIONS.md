# Evaluation bot general suggestions
A topic that was (in my humble opinion) underused in AID is `Evaluation bots`

### What are evaluation bots?
They are basically custom prompts sent along your input, and they define a structure for a kind of processing on the input/output of a prompt.  
They are used in conjunction with the "main AI", but often require lighter models to run if properly trained/finetuned to that specific use case.

### Goal
Having **importable (aka community created)** evaluation bots could greatly help community scripters and enthusiasts to implement features such as:
- [INPUT] Action difficulty detection: Detects how hard/probable your input is to inject pertinent context (i.e. "You miserably try to ___")
- [OUTPUT] Loot system: When the AI detects that an enemy died, it generates random loot (may be too much D&D for NovelAI, it's just an example)

### What should they be able to do?
Those are only propositions and very open to debate and constructive arguments
- Prompting "transient" messages that aren't part of the story (score gain, karma, etc)
- Manipulate context
  - Append text into remember section (easiest)
  - Update context 
    - Would require some sort of standard format for contextual data
    - Risks of bad update
    - Could be used to update the current location, for example
  - Remove context
    - Also risky and requires standard format

### Examples
(Noli: I have little to no idea what they actually look like in AID, so feel free to correct me)
#### Karma (input bot)
```
INPUT: You give a coin to the beggar.
OUTPUT: You gain 1 karma!
RATIONALE: Helping beggar is an act of goodness

INPUT: You kick the child in the face
OUTPUT: You lose 50 karma!
RATIONALE: Kicking children is super mean.

INPUT: You run towards the castle.
OUTPUT: No karma gained.
RATIONALE: The action was not good nor bad.
```
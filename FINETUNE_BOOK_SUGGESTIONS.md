# Book suggestions for AI finetuning

## Authors
- https://discord.com/channels/836774308772446268/837077266918932512/837646710796845097
  - Tolkien
- https://discord.com/channels/836774308772446268/837077266918932512/837523422513725450
  - Lovecraft
- https://discord.com/channels/836774308772446268/837077266918932512/837760457291989033
  - Douglas Adams
  - Terry Pratchett

## Books
- https://discord.com/channels/836774308772446268/837077266918932512/837664444820488223
  - Series
    - The Broken Earth trilogy by N.K. Jemisin (fantasy/scifi. POV:M and POV:F)
    - You/Hidden Bodies/You Love Me by Caroline Kepnes (thriller/romance. POV:F and POV:M)
    - Fighting Fantasy gamebooks https://archive.org/details/fightingfantasygamebooks.7z,  62 pdfs (fantasy/scifi)
    - Project Aon (Lone Wolf) gamebooks: https://www.projectaon.org/en/Main/AllOfTheBooks, 47 epubs (fantasy)
  - Singles
    - The Raven Tower by Ann Leckie (high fantasy. POV:M)
    - The Night Circus by Erin Morgenstern (fantasy, romance. POV:M and POV:F)
    - If on a winter's night a traveler by Italo Calvino (fictional post-WW1 country, romance. POV:M)
    - Bright Lights, Big City by Jay McInerney (80's New York, drugs, and partying. POV:M)
    - In the Dream House by Carmen Maria Machado (each chapter a different genre about crazy ex-gf who still wants you. POV:F)
- https://discord.com/channels/836774308772446268/837077266918932512/837336364755845180
  - Neil Gaiman (Stardust, Coraline, American Gods, etc)
  - Guy Gavriel Kay (trusted to help write the Silmarillion; historical-themed fiction/light fantasy)
  - Patrick Rothfuss (cliched but vivid imagery)
  - Brandon Sanderson (solid writing style; 'logical' magic systems; hundreds of books)
  - Robert Jordan (descriptive high fantasy, long series)
  - Jim Butcher (Dresden files: urban private eye/fantasy)
  - Andrzej Sapkowski (Witcher saga; why not?)
- https://discord.com/channels/836774308772446268/837077266918932512/837261227289083944
  - Naomi Novik: His Majesty's Dragon (Napoleonic Wars alt-history, dragons, politics.)
  - Katie Waitman: The Merro Tree (Science fiction, nudity, casual sex, interspecies, anti-censorship.)
  - Mercedes Lackey: The Black Gryphon (Fantasy, gryphons, casual sex, sexual healing.)
  - Iain M. Banks: Excession (Space opera, cosmic horror, AIs, sentient spaceships.)
- https://discord.com/channels/836774308772446268/837077266918932512/837760193525710868
  - Warhammer40k
- https://discord.com/channels/836774308772446268/837259157538340864/838122645441478659
  - Tui T. Sutherland: the Wings of Fire series (fantasy, dragon protagonists, humans nearly nonexistent - might be good for offsetting AI's constant need to insert humans into any scene.)
